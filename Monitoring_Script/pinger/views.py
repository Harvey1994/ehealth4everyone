from .models import Device

from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_protect

import time,threading
import requests

def status():
    while 1:
        for device in Device.objects.all():
            address = device.web_address
            try:
                res = requests.get(address) #response
                if res.status_code == 200: device.status = True; 
                else: device.status = False;  
                device.save()
            except:
                device.status = False;  
                device.save()

@csrf_protect
def index(request):
    devices = Device.objects.all()
    tmp = [ "Correct" for r_thread in threading.enumerate() if r_thread.getName() == "status"]
    if len(tmp):
        "Everything is okay"
    else:
        #A thread named status has to be created that keeps checking if the webservers are up
        new_thread = threading.Thread(target = status, name = "status"); new_thread.start()
    template = loader.get_template("pinger/index.htm")
    context = {
            "devices": devices
            }
    tmp = template.render(context,request)
    return HttpResponse(tmp)

def detail(request,name):
    device = get_object_or_404(Device,name = name); 
    return HttpResponse(device.web_address)

@csrf_protect
def add(request):
    name = request.POST.get("device name")
    address = request.POST.get("web address")
    s_time = time.time()
    new_d = Device(name = name, web_address = address, startup_time = s_time, stop_time = 0,status = False) #new device
    new_d.save()
    return HttpResponseRedirect(reverse('pinger:index',))

def remove(request,name):
    device = get_object_or_404(Device,name = name); device.delete()
    return HttpResponseRedirect(reverse('pinger:index',))

def pause(request,name):
    return HttpResponse("This Feature is not yet available")

def restart(request,name):
    return HttpResponse("This Feature is not yet available")

def rename (request,name):
    return HttpResponse("This Feature is not yet available")

def dev_log(request,name):
    return HttpResponse("This Feature is not yet available")

