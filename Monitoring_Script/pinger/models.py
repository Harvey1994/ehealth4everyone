from django.db import models

class Device(models.Model):
    name = models.CharField(max_length = 21)
    web_address = models.CharField(max_length = 50)
    startup_time = models.IntegerField()
    stop_time = models.IntegerField()
    status = models.BooleanField(default = True)
    
    def __str__(self):
        return (self.name)
    
class Log(models.Model):
    device = models.ForeignKey(Device,on_delete = models.CASCADE)
    period = models.CharField(max_length = 20)
    uptime = models.IntegerField(default = 0)
    downtime = models.IntegerField(default = 0)
    log_msg = models.CharField(max_length = 200,default = "No log created yet")
    
    def __str__(self):
        return (self.device)

